#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>

#include <iostream>
#include <fstream>
#include <chrono>

#include "SquareDetection.hpp"
#include "LeadDetection.hpp"
#include "FPSCounter.hpp"
#include "CalibrationProcedure.hpp"

std::tuple<bool, Calibration> gatherCalibrationData(cv::VideoCapture& videoCapture) {
    std::vector<std::vector<cv::Point2f> > imagePoints;
    cv::Mat cameraMatrix, distCoeffs;
    cv::Size imageSize;

    CalibrationSettings settings = { };
    settings.checkerboardSize = cv::Size(6, 6);
    settings.squareSize = 125.0f;

    int width = videoCapture.get(cv::CAP_PROP_FRAME_WIDTH);
    int height = videoCapture.get(cv::CAP_PROP_FRAME_HEIGHT);
    settings.imageSize = cv::Size(width, height);

    CalibrationProcedure procedure = { settings };

    // Buffer of reference calibration frames to calibrate onto.
    auto referenceCalibrationFrames = std::vector<std::tuple<bool, cv::Mat>> { 10 };
    for (auto& v : referenceCalibrationFrames) {
        auto& [valid, frame] = v;
        valid = videoCapture.read(frame);
        cv::imshow("Output", frame);
        cv::waitKey(1);
    }

    return procedure.calibrateFromFrames(std::move(referenceCalibrationFrames));    
}

int main(int argc, char** argv) {
    auto fpsCounter = FPSCounter();

    cv::SimpleBlobDetector::Params params;
    params.filterByArea = true;
    params.minArea = 10;
    params.maxArea = 5000;
    params.filterByInertia = false;
    params.filterByConvexity = false;
    params.filterByColor = false;
    auto leadDetection = LeadDetection(params);
    auto videoStream = cv::VideoCapture("http://pickandplace.local:9090/stream/video.mjpeg");
    
    Calibration calibration;

    Json::Value configJson;
    auto calibFile = std::ifstream("calib/primary.json");
    calibFile >> configJson;

    calibration.fromJsonValue(configJson);
    calibration.initUndistortRectifyMap();

    cv::Mat inputImage;

    while (true) {
        videoStream >> inputImage;

        //cv::Mat inputImage = cv::imread("images/diff-components.jpg");

        cv::imshow("input", inputImage);
        cv::waitKey(1);

        leadDetection.detectLeads(inputImage);

        std::cout << fpsCounter.getCount() << std::endl;
    }

    return 0;
}
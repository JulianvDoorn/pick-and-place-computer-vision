#ifndef CALIBRATION_SETTINGS_HPP
#define CALIBRATION_SETTINGS_HPP
#pragma once

#include <opencv2/core.hpp>

struct CalibrationSettings {
    cv::Size checkerboardSize;
    float squareSize;
    cv::Size imageSize;
};

#endif // CALIBRATION_SETTINGS_HPP
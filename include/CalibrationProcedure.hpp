#ifndef CALIBRATION_PROCEDURE_HPP
#define CALIBRATION_PROCEDURE_HPP
#pragma once

#include <opencv2/calib3d.hpp>
#include <opencv2/opencv.hpp>

#include "Calibration.hpp"
#include "CalibrationSettings.hpp"

class CalibrationProcedure {
    std::tuple<bool, std::vector<cv::Point2f>> findChessboardCorners(const cv::Mat& view) {
        std::vector<cv::Point2f> pointBuffer;

        std::cout << "Started calulcating..." << std::endl;

        cv::Mat grey;
        cv::cvtColor(view, grey, cv::COLOR_BGR2GRAY);
        cv::threshold(grey, grey, 100, 255, cv::THRESH_BINARY);
        cv::imshow("Calib", grey);

        bool found = cv::findChessboardCorners(
            grey, cv::Size(6, 6), pointBuffer, cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FAST_CHECK | cv::CALIB_CB_NORMALIZE_IMAGE);

        std::cout << "Found chessboard: " << found << std::endl;

        return {found, std::move(pointBuffer)};
    }

    bool performCalibrationStep(const std::vector<std::vector<cv::Point2f>>& imagePoints) {
        std::vector<cv::Mat> rvecs, tvecs;
        std::vector<float> reprojErrs;
        double totalAvgErr = 0;

        std::vector<std::vector<cv::Point3f>> objectPoints(1);
        calcBoardCornerPositions(objectPoints[0]);

        objectPoints.resize(imagePoints.size(), objectPoints[0]);

        // Find intrinsic and extrinsic camera parameters
        double rms = cv::calibrateCamera(
            objectPoints,
            imagePoints,
            settings.imageSize,
            cameraMatrix,
            distCoeffs,
            rvecs,
            tvecs,
            cv::CALIB_FIX_K4 | cv::CALIB_FIX_K5
        );

        //std::cout << "Re-projection error reported by calibrateCamera: " << rms << std::endl;

        bool ok = cv::checkRange(cameraMatrix) && cv::checkRange(distCoeffs);

        totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints, rvecs, tvecs, reprojErrs);

        //std::cout << (ok ? "Calibration succeeded" : "Calibration failed") << ". avg re projection error = " << totalAvgErr
        //          << std::endl;

        return ok;
    }


    void calcBoardCornerPositions(std::vector<cv::Point3f>& corners) {
        corners.clear();
        for (size_t i = 0; i < settings.checkerboardSize.height; i++) {
            for (size_t j = 0; j < settings.checkerboardSize.width; j++) {
                corners.push_back(cv::Point3f(float(j * settings.squareSize), float(i * settings.squareSize), 0));
            }
        }
    }

    double computeReprojectionErrors(
      const std::vector<std::vector<cv::Point3f>>& objectPoints, const std::vector<std::vector<cv::Point2f>>& imagePoints,
      const std::vector<cv::Mat>& rvecs, const std::vector<cv::Mat>& tvecs, std::vector<float>& perViewErrors) {
        std::vector<cv::Point2f> imagePoints2;
        unsigned int totalPoints = 0;
        double totalErr = 0, err;
        perViewErrors.resize(objectPoints.size());

        for (size_t i = 0; i < objectPoints.size(); i++) {
            cv::projectPoints(cv::Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix, distCoeffs, imagePoints2);
            err = cv::norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), cv::NORM_L2);

            size_t n = objectPoints[i].size();

            perViewErrors[i] = static_cast<float>(std::sqrt(err * err / n));
            totalErr += err * err;
            totalPoints += n;
        }

        return std::sqrt(totalErr / totalPoints);
    }

    Calibration generateCalibrationFromData() {
        return Calibration(cameraMatrix, distCoeffs, cv::Mat_<float>::eye(3,3), cameraMatrix, settings.imageSize, CV_16SC2);
    }

    CalibrationSettings settings;
    cv::Mat_<double> cameraMatrix = cv::Mat_<double>::eye(3, 3);
    cv::Mat_<double> distCoeffs = cv::Mat_<double>::zeros(8, 1);
    std::vector<std::tuple<bool, cv::Mat>> frames;
public:
    CalibrationProcedure(const CalibrationSettings& settings) : settings(settings) { }

    std::tuple<bool, Calibration> calibrateFromFrames(std::vector<std::tuple<bool, cv::Mat>>&& _frames) {
        frames = _frames;

        std::vector<std::vector<cv::Point2f>> chessboardPointsForEach;

        for (auto f : frames) {
            auto& [ valid, frame ] = f;

            if (!valid) {
                continue;
            }

            auto [ chessboardFound, chessboardPoints ] = findChessboardCorners(frame);

            cv::Mat calibrationView = frame.clone();
            drawPointsOntoImage(chessboardPoints, calibrationView);

            cv::imshow("Calib", calibrationView);
            cv::waitKey(100);

            if (!chessboardFound) {
                continue;
            }

            chessboardPointsForEach.emplace_back(std::move(chessboardPoints));

            performCalibrationStep(chessboardPointsForEach);
        }

        return { true, generateCalibrationFromData() };
    }

    void drawPointsOntoImage(const std::vector<cv::Point2f>& pointBuffer, cv::Mat& view) {
        drawChessboardCorners(view, cv::Size(6, 6), cv::Mat(pointBuffer), true);
    }
};

#endif // CALIBRATION_PROCEDURE_HPP
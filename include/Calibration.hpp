#ifndef CALIBRATION_HPP
#define CALIBRATION_HPP
#pragma once

#include <opencv2/core.hpp>
#include <json/json.h>
#include <iterator>
#include <stdexcept>

class Calibration {
    // Begin serializable parameters
    cv::Mat_<double> cameraMatrix;
    cv::Mat_<double> distCoeffs;
    cv::Mat_<float> R;
    cv::Mat_<double> newCameraMatrix;
    cv::Size size;
    int m1type;
    // End serializable parameters

    // mapX and mapY are not serializable because they can be generated from
    // the above parameters. And other than that it is simply binary data.
    cv::Mat mapX;
    cv::Mat mapY;

    template <class point_type>
    static point_type remapPoint(point_type point, const cv::Mat& mapX, const cv::Mat& mapY) {
        const auto remapX = mapX.at(p);
        const auto remapY = mapY.at(p);

        return { remapX, remapY };
    }

    template <class dat_type>
    static dat_type jsonValueAsType(const Json::Value& jsonVal) {
        if constexpr (std::is_same_v<dat_type, double>) {
            return jsonVal.asDouble();
        } else if constexpr (std::is_same_v<dat_type, float>) {
            return jsonVal.asFloat();
        }
    }

    template <class dat_type>
    static Json::Value matrixToJsonValue(const cv::Mat_<dat_type>& mat) {
        Json::Value jsonArr;
        Json::Value row;

        auto colCounter = 0;
        for (auto it = std::begin(mat); it != std::end(mat); it++) {
            row.append(*it);

            if (++colCounter >= mat.cols) {
                jsonArr.append(row);
                row.clear();
                colCounter = 0;
            }
        }

        return jsonArr;
    }

    template <class dat_type>
    static cv::Mat_<dat_type> jsonValueToMatrix(const Json::Value& jsonMat) {
        // Precondition: all rows are of equal length
        size_t rowSize = jsonMat[0].size();
        for (auto jsonRow : jsonMat) {
            if (jsonRow.size() != rowSize) {
                throw std::runtime_error("Precondition not met: all rows are not of equal length");
            }
        }

        auto mat = cv::Mat_<dat_type>(jsonMat.size(), rowSize);
        auto colCounter = 0;
        for (auto jsonRow : jsonMat) {
            auto rowCounter = 0;
            for (auto jsonVal : jsonRow) {
                // Note: call operator for 2D array accessing
                mat(colCounter, rowCounter++) = jsonValueAsType<dat_type>(jsonVal);
            }

            colCounter++;
        }

        return mat;
    }

public:

    Json::Value toJsonValue() {
        Json::Value root;

        root["cameraMatrix"] = matrixToJsonValue(cameraMatrix);
        root["distCoeffs"] = matrixToJsonValue(distCoeffs);
        root["R"] = matrixToJsonValue(R);
        root["newCameraMatrix"] = matrixToJsonValue(newCameraMatrix);
        Json::Value jsonSize;
        jsonSize.append(size.width);
        jsonSize.append(size.height);
        root["size"] = jsonSize;
        root["m1type"] = m1type;

        return root;
    }

    void fromJsonValue(Json::Value val) {
        cameraMatrix = jsonValueToMatrix<double>(val["cameraMatrix"]);
        distCoeffs = jsonValueToMatrix<double>(val["distCoeffs"]);
        R = jsonValueToMatrix<float>(val["R"]);
        newCameraMatrix = jsonValueToMatrix<double>(val["newCameraMatrix"]);
        size = cv::Size(val["size"][0].asInt(), val["size"][1].asInt());
        m1type = val["m1type"].asInt();
    }

    Calibration() { }

    Calibration(cv::Mat_<double> _cameraMatrix, cv::Mat_<double> _distCoeffs, cv::Mat_<float> _R, cv::Mat_<double> _newCameraMatrix, cv::Size _size, int _m1type) :
        cameraMatrix(_cameraMatrix),
        distCoeffs(_distCoeffs),
        R(_R),
        newCameraMatrix(_newCameraMatrix),
        size(_size),
        m1type(_m1type)
    {
        initUndistortRectifyMap();
    }

    void initUndistortRectifyMap() {
        // Builds a distortion correction map
        cv::initUndistortRectifyMap(cameraMatrix, distCoeffs, R, newCameraMatrix, size, m1type, mapX, mapY);
    }

    std::tuple<bool, cv::Mat> readCorrectedImage(cv::VideoCapture& capture) {
        cv::Mat readImage;
        cv::Mat correctedImage;
        
        if (!capture.read(readImage)) {
            return { false, cv::Mat() };
        }

        cv::remap(readImage, correctedImage, mapX, mapY, cv::INTER_LINEAR, cv::BORDER_CONSTANT);

        return { true, correctedImage };
    }

    template <class point_type>
    void correctPointCloud(const std::vector<point_type>& points) {
        std::vector<point_type> corrected;
        corrected.reserve(points.size());

        for (const point_type& p : points) {
            corrected.emplace_back(remapPoint(p));
        }

        return corrected;
    }

    template <class OS>
    friend OS& operator<< (OS& os, const Calibration& c) {
        os << "cameraMatrix: \n" << cv::format(c.cameraMatrix, cv::Formatter::FMT_DEFAULT) << '\n';
        os << "distCoeffs: \n" << cv::format(c.distCoeffs, cv::Formatter::FMT_DEFAULT) << '\n';
        os << "R: \n" << cv::format(c.R, cv::Formatter::FMT_DEFAULT) << '\n';
        os << "newCameraMatrix: \n" << cv::format(c.newCameraMatrix, cv::Formatter::FMT_DEFAULT) << '\n';
        os << "size: (" << c.size.width << ", " << c.size.height << ")\n";
        os << "m1type: " << c.m1type;
        return os;
    }
};

#endif // CALIBRATION_HPP
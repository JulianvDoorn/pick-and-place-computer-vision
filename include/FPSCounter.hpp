#ifndef FPS_COUNTER_HPP
#define FPS_COUNTER_HPP
#pragma once

#include <chrono>
#include <limits>

class fps_t {
    uint32_t fps;

public:
    static fps_t max() {
        return fps_t(std::numeric_limits<decltype(fps)>::max());
    }

    fps_t(uint32_t _fps) : fps(_fps) { }

    operator uint32_t() {
        return fps;
    }
};

template <class OS>
OS& operator << (OS& os, fps_t fps) {
    if (fps == 0) {
        return os << "FPS: " << static_cast<uint32_t>(fps) << " (infinite ms)";
    }

    return os << "FPS: " << static_cast<uint32_t>(fps) << " (" << static_cast<uint32_t>(1000 / fps) << "ms)";
}

class FPSCounter {
    std::chrono::system_clock::time_point lastMeasurement;

public:
    FPSCounter() : lastMeasurement(std::chrono::system_clock::now()) { }

    fps_t getCount() {
        auto now = std::chrono::system_clock::now();
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastMeasurement).count();
        lastMeasurement = now;

        if (ms == 0) {
            return fps_t::max();
        }

        return 1000 / ms;
    }
};

#endif // FPS_COUNTER
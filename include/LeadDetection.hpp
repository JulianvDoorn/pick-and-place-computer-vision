#ifndef LEAD_DETECTION_HPP
#define LEAD_DETECTION_HPP
#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <vector>

class LeadDetection {
    cv::Ptr<cv::SimpleBlobDetector> blobDetector;
    std::vector<cv::KeyPoint> keypoints;

public:
    LeadDetection(cv::SimpleBlobDetector::Params params) {
        blobDetector = cv::SimpleBlobDetector::create(params);
    }

    std::vector<cv::KeyPoint> detectLeads(cv::Mat input) {
        cv::Mat inputGrey;
        cv::cvtColor(input, inputGrey, cv::COLOR_BGR2GRAY);
        cv::imshow("grey", inputGrey);
        cv::waitKey(1);

        cv::Mat greyBlur;
        cv::GaussianBlur(inputGrey, greyBlur, cv::Size(11, 11), 5);
        cv::imshow("greyBlur", greyBlur);
        cv::waitKey(1);

        cv::Mat shapenedGrey;
        cv::addWeighted(inputGrey, 1.5, greyBlur, -0.5, 0, shapenedGrey);
        cv::imshow("greySharpened", shapenedGrey);

        cv::Mat thresh;
        cv::threshold(shapenedGrey, thresh, 100.0, 255.0, cv::THRESH_BINARY);
        //cv::adaptiveThreshold(inputGrey, thresh, 255.0, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, 15, 2);

        cv::imshow("thresh", thresh);
        cv::waitKey(1);

        auto dilation_size = 4;
        auto dilation_type = cv::MORPH_CLOSE;

        cv::Mat kernel = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(9, 9));
        cv::Mat eroded;
        cv::morphologyEx(thresh, eroded, cv::MORPH_ERODE, kernel);
        cv::imshow("eroded", eroded);

        auto start = std::chrono::system_clock::now();
        keypoints.clear();
        std::vector<cv::KeyPoint> buffer;
        blobDetector->detect(thresh, buffer);
        std::move(std::begin(buffer), std::end(buffer), std::back_inserter(keypoints));
        blobDetector->detect(eroded, buffer);
        std::move(std::begin(buffer), std::end(buffer), std::back_inserter(keypoints));
        auto end = std::chrono::system_clock::now();
        auto diffMs = end - start;
        std::cout << "Blob detection took: " << std::chrono::duration_cast<std::chrono::milliseconds>(diffMs).count() << "ms" << std::endl;

        std::cout << "Found blobs: " << keypoints.size() << std::endl;

        cv::Mat imageWithKeypoints;
        cv::drawKeypoints(input, keypoints, imageWithKeypoints, cv::Scalar(0, 0, 255), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

        cv::imshow("keypoints", imageWithKeypoints);
        cv::waitKey(1);

        return keypoints;
    }
};

#endif // LEAD_DETECTION_HPP